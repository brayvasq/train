# Python Train

A train has N carriages, each with 10 incrementally numbered seats. The train makes N stops, where a random people enter and leave.
The following operations must be done:
- Allows to know how many people enter and leave the train in total.
- Allows to know the people info with their respective seat number.
- Allows to know how many people there are for each gender.
- Allows to sort the people by age.
- Allows to know the total value payed by ticket type.
    - Type A: $2000
    - Type B: $4000
    - Type C: $6000


Implements the Factory pattern for List choosing. There are two type, self implemented and language native.

To print special character we should use Python 3 and the utf-8 coding in the top of the file

```python
# -*- coding: utf-8 -*-
```

## Create enviroment

```bash
mkdir project && cd project
virtualenv .
source bin/activate
python main.py
```

Install packages needed to build the Python library.

```bash
pip install wheel
pip install setuptools
pip install twine
```

#### See dependencies

```bash
pip freeze

html5lib==1.0.1
Pillow==7.0.0
PyPDF2==1.26.0
reportlab==3.5.34
six==1.14.0
webencodings==0.5.1
xhtml2pdf==0.2.4
```

#### Export dependences

```bash
pip freeze > requirements.txt
```

Install dependences from a file `requirements.txt`

```bash
pip install -r requirements.txt
```
