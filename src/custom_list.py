from src.node import Node

class CustomList:
    def __init__(self):
        self.head = None

    def add(self, item):
        if self.head is None:
            self.head = Node(item)
        else:
            node = self.head
            while node.next:
                node = node.next

            node.next = Node(item)

        return True

    def get(self, index):
        node = self.head
        found = False

        if index == 0:
            return self.head.value
        else:
            i = 0
            node = self.head

            while node.next and not found:
                if i == index:
                    found = True
                else:
                    node = node.next
                i += 1

        return node.value

    def delete(self,index):
        prev = None
        curr = self.head

        i = 0
        while curr.next and i != index:
            prev = curr
            curr = curr.next
            i += 1


        if prev is None:
            self.head = self.head.next
        else:
           prev.next = curr.next
           curr.next = None

        return True

    def size(self):
        node = self.head
        size = 0
        while node is not None:
            size += 1
            node = node.next

        return size

    def print(self):
        node = self.head
        string = "["
        while node:
            string += str(node.value) + ", "
            node = node.next

        string += "]"
        print(string)


# if __name__ == "__main__":
#     my_list = CustomList()
#     my_list.print()
#     my_list.add(2)
#     my_list.add(4)
#     my_list.add(5)
#     my_list.add(6)
#     my_list.add(1)
#     my_list.add(9)
#     my_list.add(0)
#     my_list.add(20)

#     my_list.print()

#     print("Size: ", my_list.size())
#     print(my_list.get(0))
#     print(my_list.get(2))
#     print(my_list.get(4))
#     print(my_list.get(5))
#     print(my_list.get(7))

#     my_list.delete(0)
#     my_list.delete(3)
#     my_list.delete(5)
#     my_list.print()
#     print(my_list.size())

