class DefaultList:
    def __init__(self):
        self.list = []

    def add(self, item):
        return self.list.append(item)

    def get(self, index):
        return self.list[index]

    def delete(self, index):
        return self.list.pop(index)

    def size(self):
        return len(self.list)

    def print(self):
        print(self.list)


# if __name__ == "__main__":
#     my_list = DefaultList()
#     my_list.print()
#     my_list.add(2)
#     my_list.add(4)
#     my_list.add(5)
#     my_list.add(6)
#     my_list.add(1)
#     my_list.add(9)
#     my_list.add(0)
#     my_list.add(20)

#     my_list.print()

#     print("Size: ", my_list.size())
#     print(my_list.get(0))
#     print(my_list.get(2))
#     print(my_list.get(4))
#     print(my_list.get(5))
#     print(my_list.get(7))

#     my_list.delete(0)
#     my_list.delete(3)
#     my_list.delete(5)
#     my_list.print()
#     print(my_list.size())
