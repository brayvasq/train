# -*- coding: utf-8 -*-
from src.king import King

class App:
    """
    App constructor, here the commands parser will
    be instanciated
    """
    def __init__(self):
        self.name = "Python train!"

    """
    This method will create the elements and simulate
    the train stops, enters and leaves
    """
    def simulate(self, custom):
        print(" ########################### SETUP ############################### ")
        king_france = King(custom, "King France")
        king_france.warrios_info()
        print()
        king_spain  = King(custom, "King Spain")
        king_spain.warrios_info()

        for _ in range(0,5):
            self.__start__fight(king_france, king_spain)

        print(" \n########################## RESULTS ############################")
        king_france.warrios_info()
        print()
        king_spain.warrios_info()

    def __start__fight(self, king_france, king_spain):
        warrior_france = king_france.select_warrior()
        warrior_spain  = king_spain.select_warrior()

        points_france = 0
        points_spain  = 0

        if warrior_france.force > warrior_spain.force:
            points_france += 1
        elif warrior_spain.force > warrior_france.force:
            points_spain += 1

        if warrior_france.resistance > warrior_spain.resistance:
            points_france += 1
        elif warrior_spain.resistance > warrior_france.resistance:
            points_spain += 1

        if warrior_france.agility > warrior_spain.agility:
            points_france += 1
        elif warrior_spain.agility > warrior_france.agility:
            points_spain += 1

        if warrior_france.intelligence > warrior_spain.intelligence:
            points_france += 1
        elif warrior_spain.intelligence > warrior_france.intelligence:
            points_spain += 1

        if warrior_france.ki > warrior_spain.ki:
            points_france += 1
        elif warrior_spain.ki > warrior_france.ki:
            points_spain += 1

        if points_france > points_spain:
            king_spain.remove_warrior(warrior_spain)
        elif points_spain > points_france:
            king_france.remove_warrior(warrior_france)

        print("\n Fight ======>")
        print(warrior_spain, " VS. ", warrior_france)

