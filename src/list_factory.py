from src.custom_list import CustomList
from src.default_list import DefaultList

class ListFactory:
    def __init__(self, type):
        self.type = type


    def create_list(self):
        list = None
        if self.type in ["-c", "--custom"]:
            print( " =================== CUSTOM LIST ======================")
            list = CustomList()
        else:
            print( " =================== DEFAULT LIST ======================")
            list = DefaultList()

        return list
