# -*- coding: utf-8 -*-
import random

class Warrior:
    def __init__(self, num, king):
        self.number = num
        self.name = "Warrior: " + str(num) + " From: " + king
        self.character = random.choices(self.character_list())
        self.__setup_skils()

    # Public methods
    def info(self):
        print(self)

        info = ""
        info += "Force: " + str(self.force) + " Resistance: " + str(self.resistance)
        info += " Agility: " + str(self.agility) + " Intelligence: " + str(self.intelligence)
        info += " Ki: " + str(self.ki)

        print(info)

    # Private Methods
    def __setup_skils(self):
        self.force = random.randrange(0, 100)
        self.resistance = random.randrange(0, 100)
        self.agility = random.randrange(0, 100)
        self.intelligence = random.randrange(0, 100)
        self.ki = random.randrange(0, 100)

    def character_list(self):
        characters = [
            "⤜(ʘ_ʘ)⤏",
            "(づ◔ ͜ʖ◔)づ",
            "ᕙ( ﾟω ﾟ)ᕗ",
            "(งσ◞ σ)ง",
            "\(▰ ʖ̯▰)/",
            "(づ ͡⎚⍊ ͡⎚)づ",
            "ᕦ(☉ ͟ʖ☉)ᕥ",
            "乁(■ѽ■)ㄏ",
            "(╭☞꘠ ʖ̯꘠)╭☞"
        ]

        return characters

    def __str__(self):
        return "Name: "+ str(self.name) + " Character : " + str(self.character)
