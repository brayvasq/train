# -*- coding: utf-8 -*-
from src.app import App
import sys, getopt

class MainApp:
    def __init__(self):
        self.app = App()

    def route_input(self, input):
        option = input[1] if len(input) > 1 else ""

        if option in ["-s", "--simulate"]:
            custom = input[2] if len(input) > 2 else "--default"
            print("Simulate")
            self.app.simulate(custom)
        elif option in ["-r", "--relp"]:
            self.repl()
        else:
            self.help()

    def repl(self):
        pass

    def help(self):
        print("The Python train!")
        print("A simple command exercise made in python")
        print("")
        print("-s, --simulate Start the program and simulates the train")
        print("    -c, --custom Uses a custom implementation of Lists")
        print("")
        print("-r, --repl Starts an interactive shell")


if __name__ == "__main__":
    MainApp().route_input(sys.argv)

