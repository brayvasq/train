# -*- coding: utf-8 -*-
import random
from src.warrior import Warrior
from src.list_factory import ListFactory

class King:
    def __init__(self, custom, name):
        self.name = name
        # self.warriors = []
        self.warriors = ListFactory(custom).create_list()
        self.setup_warriors()

    def setup_warriors(self):
        for i in range(0, 5):
            # self.warriors.append(Warrior(i, self.name))
            self.warriors.add(Warrior(i, self.name))

    def warrios_info(self):
        print("Name: ", self.name)
        for i in range(0, self.warriors.size()):
            self.warriors.get(i).info()
        # for i in self.warriors:
        #     i.info()

    def select_warrior(self):
        i = 0
        warrior = None

        while not warrior and i < self.warriors.size():
            choice = random.choice([True, False])
            if choice and self.warriors.get(i) is not None:
                warrior = self.warriors.get(i)
            i += 1

        if not warrior:
            warrior = self.warriors.get(0)

        return warrior
        # i = 0
        # warrior = None

        # while (warrior is None) and i < len(self.warriors):
        #     choice = random.choice([True, False])
        #     if choice and (self.warriors[i] is not None):
        #         warrior = self.warriors[i]
        #     i += 1

        # if warrior is None:
        #     warrior = self.warriors[0]

        # return warrior

    def remove_warrior(self, warrior):
        found = False
        i = 0

        while not found and i < self.warriors.size():
            temp_warrior = self.warriors.get(i)
            if temp_warrior.number == warrior.number:
                found = True
                self.warriors.delete(i)
            i += 1

        return found

        # while not found and i < len(self.warriors):
        #     if self.warriors[i].number == warrior.number:
        #         found = True
        #         self.warriors.pop(i)
        #     i += 1

        # # print(found, len(self.warriors))
        # return found
